# Proxy Auth Middleware for Go-Json-Rest

Middleware to proxy an authentication request to an external auth service / api.

## License

The MIT License (MIT). See LICENSE for more information.