package proxyauth

import (
	"log"
	"testing"

	"github.com/ant0ine/go-json-rest/rest"
	"github.com/ant0ine/go-json-rest/rest/test"
)

func TestAuthBasic(t *testing.T) {
	// the middleware to test
	authMiddleware := &Middleware{
		Target: "http://localhost/",
		Authorizator: func(data []byte, request *rest.Request) bool {
			log.Println("here")
			if request.Method == "GET" {
				return true
			}
			return false
		},
	}

	// api for testing failure
	apiFailure := rest.NewApi()
	apiFailure.Use(authMiddleware)
	apiFailure.SetApp(rest.AppSimple(func(w rest.ResponseWriter, r *rest.Request) {
		t.Error("Should never be executed")
	}))
	handler := apiFailure.MakeHandler()

	// simple request fails
	//recorded := test.RunRequest(t, handler, test.MakeSimpleRequest("GET", "http://localhost/", nil))
	//recorded.CodeIs(401)
	//recorded.ContentTypeIsJson()

	// auth fails without Authorization header
	//wrongCredReq := test.MakeSimpleRequest("GET", "http://localhost/", nil)
	//wrongCredReq.Header.Set("Authorization", "")
	//recorded = test.RunRequest(t, handler, wrongCredReq)
	//recorded.CodeIs(401)
	//recorded.ContentTypeIsJson()

	// auth with right cred and wrong method fails
	rightCredReq := test.MakeSimpleRequest("POST", "http://localhost/", nil)
	rightCredReq.Header.Set("Authorization", "Bearer 12345")
	recorded := test.RunRequest(t, handler, rightCredReq)
	recorded.CodeIs(500)
	recorded.ContentTypeIsJson()

	// TODO: add tests for external auth server
}
