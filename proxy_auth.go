package proxyauth

import (
	"io/ioutil"
	"net/http"

	"github.com/ant0ine/go-json-rest/rest"
)

// Middleware provides a jwt proxied authentication. On failure, a 401 HTTP response
// is returned. On success, the wrapped middleware is called, and the raw user data is
// made available as request.Env["USER_DATA"].(interface)
type Middleware struct {
	// Target url to get user data using Authorization header
	Target string

	// Callback function that should perform the authorization of the authenticated user.
	// Called only when authentication was successful. Must return true on success,
	// false on failure. Optional, default to success.
	Authorizator func(data []byte, request *rest.Request) bool
}

// MiddlewareFunc makes Middleware implement the Middleware interface.
func (mw *Middleware) MiddlewareFunc(handler rest.HandlerFunc) rest.HandlerFunc {
	if mw.Authorizator == nil {
		mw.Authorizator = func(data []byte, request *rest.Request) bool {
			return true
		}
	}

	return func(writer rest.ResponseWriter, request *rest.Request) { mw.middlewareImpl(writer, request, handler) }
}

func (mw *Middleware) middlewareImpl(writer rest.ResponseWriter, request *rest.Request, handler rest.HandlerFunc) {
	var data []byte
	if mw.Target != "" {
		authHeader := request.Header.Get("Authorization")

		req, _ := http.NewRequest("GET", mw.Target, nil)
		req.Header.Set("Authorization", authHeader)

		client := &http.Client{}
		resp, err := client.Do(req)

		if resp != nil && resp.Body != nil {
			defer resp.Body.Close()
		}

		if err != nil {
			rest.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}

		data, _ = ioutil.ReadAll(resp.Body)
		request.Env["USER_DATA"] = data
	}

	if !mw.Authorizator(data, request) {
		mw.unauthorized(writer)
		return
	}

	handler(writer, request)
}

func (mw *Middleware) unauthorized(writer rest.ResponseWriter) {
	rest.Error(writer, "Not Authorized", http.StatusUnauthorized)
}
